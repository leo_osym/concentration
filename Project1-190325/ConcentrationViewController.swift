//
//  ViewController.swift
//  Project1-190325
//
//  Created by user153841 on 3/26/19.
//  Copyright © 2019 user153841. All rights reserved.
//

import UIKit

class ConcentrationViewController: UIViewController {
    
    var numberOfPairsOfCards: Int {
        return (visibleCardButtons.count+1)/2;
    }
    
    private lazy var game = Concentration(numberOfPairsOfCards: numberOfPairsOfCards)
    
    private lazy var emojiChoices = theme ?? "🧙‍♂️🙀🎃😈👻🍎🦇💀👽🧟‍♂️"
	
    @IBOutlet private var cardButtons: [UIButton]!
    
    private var visibleCardButtons: [UIButton]! {
        return cardButtons?.filter{ !$0.superview!.isHidden }
    }
    
    @IBOutlet private weak var flipCountLabelLandscape: UILabel!
    
    @IBOutlet private weak var flipCountLabel: UILabel!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateViewFromModel()
    }
    
    @IBAction private func touchCard(_ sender: UIButton) {
        if let cardNumber = visibleCardButtons.index(of: sender){
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
            var text = "Flips:\n\(game.flips)\nScored:\n\(game.points)"
            if game.matches == (visibleCardButtons.count+1)/2 {
                text = "You win!\nScored:\n\(game.points)"
            }
            printOnLabel(text: text)
        }
    }
    private func printOnLabel(text: String) {
        var portraitText = text
        let attributes: [NSAttributedString.Key: Any] = [
            .strokeWidth: 5.0,
            .strokeColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        ]
        portraitText = text.replacingOccurrences(of: "\n", with: " ")
        let portraitString = NSAttributedString(string: portraitText, attributes: attributes)
        flipCountLabel.attributedText = portraitString
        let landscapeString = NSAttributedString(string: text, attributes: attributes)
        flipCountLabelLandscape.attributedText = landscapeString
    }
    
    @IBAction private func startNewGame(_ sender: UIButton) {
        game = Concentration(numberOfPairsOfCards: numberOfPairsOfCards)
        emojiChoices = theme ?? "🧙‍♂️🙀🎃😈👻🍎🦇💀👽🧟‍♂️"
        printOnLabel(text: "Flips:\n\(game.flips)\nScored:\n\(game.points)")
        for button in visibleCardButtons{
            button.setTitle("", for: UIControl.State.normal)
            button.backgroundColor = #colorLiteral(red: 0.476841867, green: 0.5048075914, blue: 1, alpha: 1)
        }
        navigationController?.popViewController(animated: true)
    }
    
    private func updateViewFromModel(){
        if visibleCardButtons != nil {
            for index in visibleCardButtons.indices{
                let button = visibleCardButtons[index]
                let card = game.cards[index]
                if card.isFaceUp{
                    button.setTitle(emoji(for: card), for: UIControl.State.normal)
                    button.backgroundColor = #colorLiteral(red: 0.4508578777, green: 0.9882974029, blue: 0.8376303315, alpha: 1)
                } else {
                    button.setTitle("", for: UIControl.State.normal)
                    button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0) :#colorLiteral(red: 0.476841867, green: 0.5048075914, blue: 1, alpha: 1)
                }
            }
        }
    }
    
    var theme: String? {
        didSet {
            emojiChoices = theme ?? ""
            emoji = [:]
            updateViewFromModel()
        }
    }
    
    private var emoji = [Card: String]()
    
    private func emoji(for card: Card)->String {
        if emoji[card] == nil, emojiChoices.count>0 {
            let randomStringIndex = emojiChoices.index(emojiChoices.startIndex, offsetBy: emojiChoices.count.arc4random)
            emoji[card] = String(emojiChoices.remove(at: randomStringIndex))
        }
        return emoji[card] ?? "?"
    }
}

extension Int {
    var arc4random: Int{
        if self>0{
            return Int(arc4random_uniform(UInt32(self)))
        } else if self<0{
            return -Int(arc4random_uniform(UInt32(abs(self))))
        } else {
            return 0
        }
    }
}
