//
//  Concentration.swift
//  Project1-190325
//
//  Created by user153841 on 3/27/19.
//  Copyright © 2019 user153841. All rights reserved.
//

import Foundation

struct Concentration
{
    private(set) var cards = [Card]()
    
    private var indexOfOneOnlyFaceUpCard: Int?{
        get{
            return cards.indices.filter{ cards[$0].isFaceUp }.oneAndOnly
        }
        set(newValue) {
            for index in cards.indices{
                cards[index].isFaceUp = (index == newValue)
            }
        }
    }
    
    
    private(set) var matches = 0
    private(set) var flips = 0
    private(set) var points = 0
    private(set) var themeNumber = 0
    
    mutating func chooseCard(at index: Int){
        assert(cards.indices.contains(index), "Concentration.chooseCard(at: \(index)): index is not in cards!")
        flips += 1
        if !cards[index].isMatched{
            if let matchIndex = indexOfOneOnlyFaceUpCard, matchIndex != index {
                if cards[matchIndex] == 	cards[index] {
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                    //if matched - set points
                    matches += 1
                    points += 2
                } else {
                    //if not matched - set penalty
                    if matches > 0 && (cards[matchIndex].isSeen || cards[index].isSeen){
                        points -= 1
                    }
                    cards[matchIndex].isSeen = true
                    cards[index].isSeen = true
                }
                cards[index].isFaceUp = true
            } else{
                indexOfOneOnlyFaceUpCard = index
            }
        }
    }
    
    init(numberOfPairsOfCards: Int) {
        assert(numberOfPairsOfCards > 0, "Concentration.init(at: \(numberOfPairsOfCards)): you must have at least one pair of cards!")
        for _ in 1...numberOfPairsOfCards {
            let card = Card()
            cards += [card, card]
        }
        cards = shuffleCards(in: cards)
    }
    
    private func shuffleCards(in cards: [Card]) -> [Card]{
        var shuffledCards = cards
        for i in shuffledCards.indices{
            let random = Int(arc4random_uniform(UInt32(cards.count)))
            (shuffledCards[i],shuffledCards[random]) = (shuffledCards[random],shuffledCards[i])
        }
        return shuffledCards
    }
}

extension Collection {
    var oneAndOnly: Element? {
        return count == 1 ? first : nil
    }
}
